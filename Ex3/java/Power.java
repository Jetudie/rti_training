

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from .idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

import com.rti.dds.infrastructure.*;
import com.rti.dds.infrastructure.Copyable;
import java.io.Serializable;
import com.rti.dds.cdr.CdrHelper;

public class Power   implements Copyable, Serializable{

    public int id= 0;
    public float voltage= 0;
    public float current= 0;
    public String status=  "" ; /* maximum length = (255) */

    public Power() {

    }
    public Power (Power other) {

        this();
        copy_from(other);
    }

    public static Object create() {

        Power self;
        self = new  Power();
        self.clear();
        return self;

    }

    public void clear() {

        id= 0;
        voltage= 0;
        current= 0;
        status=  ""; 
    }

    public boolean equals(Object o) {

        if (o == null) {
            return false;
        }        

        if(getClass() != o.getClass()) {
            return false;
        }

        Power otherObj = (Power)o;

        if(id != otherObj.id) {
            return false;
        }
        if(voltage != otherObj.voltage) {
            return false;
        }
        if(current != otherObj.current) {
            return false;
        }
        if(!status.equals(otherObj.status)) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        int __result = 0;
        __result += (int)id;
        __result += (int)voltage;
        __result += (int)current;
        __result += status.hashCode(); 
        return __result;
    }

    /**
    * This is the implementation of the <code>Copyable</code> interface.
    * This method will perform a deep copy of <code>src</code>
    * This method could be placed into <code>PowerTypeSupport</code>
    * rather than here by using the <code>-noCopyable</code> option
    * to rtiddsgen.
    * 
    * @param src The Object which contains the data to be copied.
    * @return Returns <code>this</code>.
    * @exception NullPointerException If <code>src</code> is null.
    * @exception ClassCastException If <code>src</code> is not the 
    * same type as <code>this</code>.
    * @see com.rti.dds.infrastructure.Copyable#copy_from(java.lang.Object)
    */
    public Object copy_from(Object src) {

        Power typedSrc = (Power) src;
        Power typedDst = this;

        typedDst.id = typedSrc.id;
        typedDst.voltage = typedSrc.voltage;
        typedDst.current = typedSrc.current;
        typedDst.status = typedSrc.status;

        return this;
    }

    public String toString(){
        return toString("", 0);
    }

    public String toString(String desc, int indent) {
        StringBuffer strBuffer = new StringBuffer();        

        if (desc != null) {
            CdrHelper.printIndent(strBuffer, indent);
            strBuffer.append(desc).append(":\n");
        }

        CdrHelper.printIndent(strBuffer, indent+1);        
        strBuffer.append("id: ").append(id).append("\n");  
        CdrHelper.printIndent(strBuffer, indent+1);        
        strBuffer.append("voltage: ").append(voltage).append("\n");  
        CdrHelper.printIndent(strBuffer, indent+1);        
        strBuffer.append("current: ").append(current).append("\n");  
        CdrHelper.printIndent(strBuffer, indent+1);        
        strBuffer.append("status: ").append(status).append("\n");  

        return strBuffer.toString();
    }

}
