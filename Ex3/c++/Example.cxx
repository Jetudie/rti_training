

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include <iosfwd>
#include <iomanip>
#include "Example.hpp"
#include "ExampleImplPlugin.h"

#include <rti/util/ostream_operators.hpp>

// ---- Power: 

Power::Power() :
    m_id_ (0) ,
    m_voltage_ (0.0f) ,
    m_current_ (0.0f) {
}   

Power::Power (
    int32_t id_param,
    float voltage_param,
    float current_param,
    const dds::core::string& status_param)
    :
        m_id_( id_param ),
        m_voltage_( voltage_param ),
        m_current_( current_param ),
        m_status_( status_param ) {
}
#ifdef RTI_CXX11_RVALUE_REFERENCES
#ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
Power::Power(Power&& other_) OMG_NOEXCEPT  :m_id_ (std::move(other_.m_id_))
,
m_voltage_ (std::move(other_.m_voltage_))
,
m_current_ (std::move(other_.m_current_))
,
m_status_ (std::move(other_.m_status_))
{
} 

Power& Power::operator=(Power&&  other_) OMG_NOEXCEPT {
    Power tmp(std::move(other_));
    swap(tmp); 
    return *this;
}
#endif
#endif   

void Power::swap(Power& other_)  OMG_NOEXCEPT 
{
    using std::swap;
    swap(m_id_, other_.m_id_);
    swap(m_voltage_, other_.m_voltage_);
    swap(m_current_, other_.m_current_);
    swap(m_status_, other_.m_status_);
}  

bool Power::operator == (const Power& other_) const {
    if (m_id_ != other_.m_id_) {
        return false;
    }
    if (m_voltage_ != other_.m_voltage_) {
        return false;
    }
    if (m_current_ != other_.m_current_) {
        return false;
    }
    if (m_status_ != other_.m_status_) {
        return false;
    }
    return true;
}
bool Power::operator != (const Power& other_) const {
    return !this->operator ==(other_);
}

// --- Getters and Setters: -------------------------------------------------
int32_t Power::id() const OMG_NOEXCEPT{
    return m_id_;
}

void Power::id(int32_t value) {
    m_id_ = value;
}

float Power::voltage() const OMG_NOEXCEPT{
    return m_voltage_;
}

void Power::voltage(float value) {
    m_voltage_ = value;
}

float Power::current() const OMG_NOEXCEPT{
    return m_current_;
}

void Power::current(float value) {
    m_current_ = value;
}

dds::core::string& Power::status() OMG_NOEXCEPT {
    return m_status_;
}

const dds::core::string& Power::status() const OMG_NOEXCEPT {
    return m_status_;
}

void Power::status(const dds::core::string& value) {
    m_status_ = value;
}

std::ostream& operator << (std::ostream& o,const Power& sample)
{
    rti::util::StreamFlagSaver flag_saver (o);
    o <<"[";
    o << "id: " << sample.id()<<", ";
    o << "voltage: " << std::setprecision(9) <<sample.voltage()<<", ";
    o << "current: " << std::setprecision(9) <<sample.current()<<", ";
    o << "status: " << sample.status() ;
    o <<"]";
    return o;
}

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        const dds::core::xtypes::StructType& dynamic_type<Power>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(Power_c_get_typecode())));
        }

    }
}  

namespace dds { 
    namespace topic {
        void topic_type_support<Power>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name){

            rti::domain::register_type_plugin(
                participant,
                type_name,
                Power_cPlugin_new,
                Power_cPlugin_delete);
        }

        void topic_type_support<Power>::initialize_sample(Power& sample){

            Power_c* native_sample=reinterpret_cast<Power_c*> (&sample);

            struct DDS_TypeDeallocationParams_t deAllocParams = {RTI_FALSE, RTI_FALSE};
            Power_c_finalize_w_params(native_sample,&deAllocParams);

            struct DDS_TypeAllocationParams_t allocParams = {RTI_FALSE, RTI_FALSE, RTI_TRUE}; 
            RTIBool ok=Power_c_initialize_w_params(native_sample,&allocParams);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to initialize_w_params");

        } 

        std::vector<char>& topic_type_support<Power>::to_cdr_buffer(
            std::vector<char>& buffer, const Power& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = Power_cPlugin_serialize_to_cdr_buffer(
                NULL, &length,reinterpret_cast<const Power_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = Power_cPlugin_serialize_to_cdr_buffer(
                &buffer[0], &length, reinterpret_cast<const Power_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;

        }

        void topic_type_support<Power>::from_cdr_buffer(Power& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = Power_cPlugin_deserialize_from_cdr_buffer(
                reinterpret_cast<Power_c*> (&sample), &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to create Power from cdr buffer");
        }

    }
}  

