

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef Example_276139096_hpp
#define Example_276139096_hpp

#include <iosfwd>
#include "ExampleImpl.h"

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport __declspec(dllexport)
#endif

#include "dds/domain/DomainParticipant.hpp"
#include "dds/topic/TopicTraits.hpp"
#include "dds/core/SafeEnumeration.hpp"
#include "dds/core/String.hpp"
#include "dds/core/array.hpp"
#include "dds/core/vector.hpp"
#include "dds/core/Optional.hpp"
#include "dds/core/xtypes/DynamicType.hpp"
#include "dds/core/xtypes/StructType.hpp"
#include "dds/core/xtypes/UnionType.hpp"
#include "dds/core/xtypes/EnumType.hpp"
#include "dds/core/xtypes/AliasType.hpp"
#include "rti/core/array.hpp"
#include "rti/util/StreamFlagSaver.hpp"
#include "rti/domain/PluginSupport.hpp"
#include "rti/core/LongDouble.hpp"
#include "rti/core/Pointer.hpp"
#include "rti/topic/TopicTraits.hpp"
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

class NDDSUSERDllExport Power {

  public:
    Power();

    Power(
        int32_t id_param,
        float voltage_param,
        float current_param,
        const dds::core::string& status_param);

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    Power (Power&&) = default;
    Power& operator=(Power&&) = default;
    Power& operator=(const Power&) = default;
    Power(const Power&) = default;
    #else
    Power(Power&& other_) OMG_NOEXCEPT;  
    Power& operator=(Power&&  other_) OMG_NOEXCEPT;
    #endif
    #endif 

    int32_t id() const OMG_NOEXCEPT;
    void id(int32_t value);

    float voltage() const OMG_NOEXCEPT;
    void voltage(float value);

    float current() const OMG_NOEXCEPT;
    void current(float value);

    dds::core::string& status() OMG_NOEXCEPT; 
    const dds::core::string& status() const OMG_NOEXCEPT;
    void status(const dds::core::string& value);

    bool operator == (const Power& other_) const;
    bool operator != (const Power& other_) const;

    void swap(Power& other_) OMG_NOEXCEPT;

  private:

    int32_t m_id_;
    float m_voltage_;
    float m_current_;
    dds::core::string m_status_;

};

inline void swap(Power& a, Power& b)  OMG_NOEXCEPT 
{
    a.swap(b);
}

NDDSUSERDllExport std::ostream& operator<<(std::ostream& o,const Power& sample);

namespace dds { 
    namespace topic {

        template<>
        struct topic_type_name<Power> {
            NDDSUSERDllExport static std::string value() {
                return "Power";
            }
        };

        template<>
        struct is_topic_type<Power> : public dds::core::true_type {};

        template<>
        struct topic_type_support<Power> {

            NDDSUSERDllExport static void initialize_sample(Power& sample);

            NDDSUSERDllExport static void register_type(
                dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, const Power& sample);

            NDDSUSERDllExport static void from_cdr_buffer(Power& sample, const std::vector<char>& buffer);

            static const rti::topic::TypePluginKind::type type_plugin_kind = 
            rti::topic::TypePluginKind::NON_STL;
        };

    }
}

namespace rti { 
    namespace topic {
        template<>
        struct dynamic_type<Power> {
            typedef dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const dds::core::xtypes::StructType& get();
        };

        template<>
        struct impl_type<Power> {
            typedef Power_c type;
        };

    }
}

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif // Example_276139096_hpp

