

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from ExampleImpl.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef ExampleImpl_276139096_h
#define ExampleImpl_276139096_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_c_h
#include "ndds/ndds_c.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern const char *Power_cTYPENAME;

typedef struct Power_c {

    DDS_Long   id ;
    DDS_Float   voltage ;
    DDS_Float   current ;
    DDS_Char *   status ;

    Power_c() {}

} Power_c ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Power_c_get_typecode(void); /* Type code */

DDS_SEQUENCE(Power_cSeq, Power_c);

NDDSUSERDllExport
RTIBool Power_c_initialize(
    Power_c* self);

NDDSUSERDllExport
RTIBool Power_c_initialize_ex(
    Power_c* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Power_c_initialize_w_params(
    Power_c* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void Power_c_finalize(
    Power_c* self);

NDDSUSERDllExport
void Power_c_finalize_ex(
    Power_c* self,RTIBool deletePointers);

NDDSUSERDllExport
void Power_c_finalize_w_params(
    Power_c* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Power_c_finalize_optional_members(
    Power_c* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Power_c_copy(
    Power_c* dst,
    const Power_c* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* ExampleImpl */

