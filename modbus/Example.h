

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef Example_276139147_h
#define Example_276139147_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_c_h
#include "ndds/ndds_c.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern const char *MeterTYPENAME;

typedef struct Meter {

    DDS_Long   id ;
    DDS_Float   voltage ;
    DDS_Float   current ;
    DDS_Float   power ;
    DDS_Float   frequency ;
    DDS_Float   pf ;
    DDS_Char *   status ;

} Meter ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Meter_get_typecode(void); /* Type code */

DDS_SEQUENCE(MeterSeq, Meter);

NDDSUSERDllExport
RTIBool Meter_initialize(
    Meter* self);

NDDSUSERDllExport
RTIBool Meter_initialize_ex(
    Meter* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Meter_initialize_w_params(
    Meter* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void Meter_finalize(
    Meter* self);

NDDSUSERDllExport
void Meter_finalize_ex(
    Meter* self,RTIBool deletePointers);

NDDSUSERDllExport
void Meter_finalize_w_params(
    Meter* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Meter_finalize_optional_members(
    Meter* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Meter_copy(
    Meter* dst,
    const Meter* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* Example */

