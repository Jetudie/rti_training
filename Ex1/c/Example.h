

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef Example_276139088_h
#define Example_276139088_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_c_h
#include "ndds/ndds_c.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern const char *PowerTYPENAME;

typedef struct Power {

    DDS_Long   id ;
    DDS_Float   voltage ;
    DDS_Float   current ;
    DDS_Char *   status ;

} Power ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Power_get_typecode(void); /* Type code */

DDS_SEQUENCE(PowerSeq, Power);

NDDSUSERDllExport
RTIBool Power_initialize(
    Power* self);

NDDSUSERDllExport
RTIBool Power_initialize_ex(
    Power* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Power_initialize_w_params(
    Power* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void Power_finalize(
    Power* self);

NDDSUSERDllExport
void Power_finalize_ex(
    Power* self,RTIBool deletePointers);

NDDSUSERDllExport
void Power_finalize_w_params(
    Power* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Power_finalize_optional_members(
    Power* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Power_copy(
    Power* dst,
    const Power* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* Example */

