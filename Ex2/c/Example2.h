

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example2.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef Example2_31567102_h
#define Example2_31567102_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_c_h
#include "ndds/ndds_c.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern const char *ICLabTYPENAME;

typedef struct ICLab {

    DDS_UnsignedShort   num_of_student ;
    DDS_Char *   head ;
    DDS_Char *   pet ;
    struct    DDS_StringSeq  student ;

} ICLab ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* ICLab_get_typecode(void); /* Type code */

DDS_SEQUENCE(ICLabSeq, ICLab);

NDDSUSERDllExport
RTIBool ICLab_initialize(
    ICLab* self);

NDDSUSERDllExport
RTIBool ICLab_initialize_ex(
    ICLab* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool ICLab_initialize_w_params(
    ICLab* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void ICLab_finalize(
    ICLab* self);

NDDSUSERDllExport
void ICLab_finalize_ex(
    ICLab* self,RTIBool deletePointers);

NDDSUSERDllExport
void ICLab_finalize_w_params(
    ICLab* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void ICLab_finalize_optional_members(
    ICLab* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool ICLab_copy(
    ICLab* dst,
    const ICLab* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* Example2 */

