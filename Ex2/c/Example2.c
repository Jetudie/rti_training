

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example2.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_c_h
#include "ndds/ndds_c.h"
#endif

#ifndef cdr_type_h
#include "cdr/cdr_type.h"
#endif    

#ifndef osapi_heap_h
#include "osapi/osapi_heap.h" 
#endif
#else
#include "ndds_standalone_type.h"
#endif

#include "Example2.h"

/* ========================================================================= */
const char *ICLabTYPENAME = "ICLab";

DDS_TypeCode* ICLab_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode ICLab_g_tc_head_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode ICLab_g_tc_pet_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode ICLab_g_tc_student_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode ICLab_g_tc_student_sequence = DDS_INITIALIZE_SEQUENCE_TYPECODE((12),NULL);
    static DDS_TypeCode_Member ICLab_g_tc_members[4]=
    {

        {
            (char *)"num_of_student",/* Member name */
            {
                0,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"head",/* Member name */
            {
                1,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"pet",/* Member name */
            {
                2,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"student",/* Member name */
            {
                3,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode ICLab_g_tc =
    {{
            DDS_TK_STRUCT,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"ICLab", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            4, /* Number of members */
            ICLab_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for ICLab*/

    if (is_initialized) {
        return &ICLab_g_tc;
    }

    ICLab_g_tc_student_sequence._data._typeCode = (RTICdrTypeCode *)&ICLab_g_tc_student_string;

    ICLab_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_ushort;

    ICLab_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&ICLab_g_tc_head_string;

    ICLab_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&ICLab_g_tc_pet_string;

    ICLab_g_tc_members[3]._representation._typeCode = (RTICdrTypeCode *)& ICLab_g_tc_student_sequence;

    is_initialized = RTI_TRUE;

    return &ICLab_g_tc;
}

RTIBool ICLab_initialize(
    ICLab* sample) {
    return ICLab_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool ICLab_initialize_ex(
    ICLab* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return ICLab_initialize_w_params(
        sample,&allocParams);

}

RTIBool ICLab_initialize_w_params(
    ICLab* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    void* buffer = NULL;
    if (buffer) {} /* To avoid warnings */

    if (sample == NULL) {
        return RTI_FALSE;
    }
    if (allocParams == NULL) {
        return RTI_FALSE;
    }

    if (!RTICdrType_initUnsignedShort(&sample->num_of_student)) {
        return RTI_FALSE;
    }

    if (allocParams->allocate_memory){
        sample->head= DDS_String_alloc ((255));
        if (sample->head == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->head!= NULL) { 
            sample->head[0] = '\0';
        }
    }

    if (allocParams->allocate_memory){
        sample->pet= DDS_String_alloc ((255));
        if (sample->pet == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->pet!= NULL) { 
            sample->pet[0] = '\0';
        }
    }

    if (allocParams->allocate_memory) {
        DDS_StringSeq_initialize(&sample->student  );
        DDS_StringSeq_set_absolute_maximum(&sample->student , (12));
        if (!DDS_StringSeq_set_maximum(&sample->student , (12))) {
            return RTI_FALSE;
        }
        buffer = DDS_StringSeq_get_contiguous_bufferI(
            &sample->student );

        if (buffer != NULL) {
            if (!RTICdrType_initStringArray(buffer, 
            (12),
            (255)+1,
            RTI_CDR_CHAR_TYPE)) {
                return RTI_FALSE;
            } 
        }
    } else { 
        DDS_StringSeq_set_length(&sample->student, 0);
    }
    return RTI_TRUE;
}

void ICLab_finalize(
    ICLab* sample)
{

    ICLab_finalize_ex(sample,RTI_TRUE);
}

void ICLab_finalize_ex(
    ICLab* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    ICLab_finalize_w_params(
        sample,&deallocParams);
}

void ICLab_finalize_w_params(
    ICLab* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }

    if (deallocParams == NULL) {
        return;
    }

    if (sample->head != NULL) {
        DDS_String_free(sample->head);
        sample->head=NULL;

    }
    if (sample->pet != NULL) {
        DDS_String_free(sample->pet);
        sample->pet=NULL;

    }
    DDS_StringSeq_finalize(&sample->student);

}

void ICLab_finalize_optional_members(
    ICLab* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool ICLab_copy(
    ICLab* dst,
    const ICLab* src)
{

    if (dst == NULL || src == NULL) {
        return RTI_FALSE;
    }

    if (!RTICdrType_copyUnsignedShort (
        &dst->num_of_student, &src->num_of_student)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->head, src->head, 
        (255) + 1, RTI_FALSE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->pet, src->pet, 
        (255) + 1, RTI_FALSE)){
        return RTI_FALSE;
    }
    if (!DDS_StringSeq_copy(&dst->student ,
    &src->student )) {
        return RTI_FALSE;
    }

    return RTI_TRUE;

}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'ICLab' sequence class.
*/
#define T ICLab
#define TSeq ICLabSeq

#define T_initialize_w_params ICLab_initialize_w_params

#define T_finalize_w_params   ICLab_finalize_w_params
#define T_copy       ICLab_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params

#undef T_initialize_w_params

#undef TSeq
#undef T

