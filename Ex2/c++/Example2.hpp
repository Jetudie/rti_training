

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example2.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef Example2_31567102_hpp
#define Example2_31567102_hpp

#include <iosfwd>
#include "Example2Impl.h"

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport __declspec(dllexport)
#endif

#include "dds/domain/DomainParticipant.hpp"
#include "dds/topic/TopicTraits.hpp"
#include "dds/core/SafeEnumeration.hpp"
#include "dds/core/String.hpp"
#include "dds/core/array.hpp"
#include "dds/core/vector.hpp"
#include "dds/core/Optional.hpp"
#include "dds/core/xtypes/DynamicType.hpp"
#include "dds/core/xtypes/StructType.hpp"
#include "dds/core/xtypes/UnionType.hpp"
#include "dds/core/xtypes/EnumType.hpp"
#include "dds/core/xtypes/AliasType.hpp"
#include "rti/core/array.hpp"
#include "rti/util/StreamFlagSaver.hpp"
#include "rti/domain/PluginSupport.hpp"
#include "rti/core/LongDouble.hpp"
#include "rti/core/Pointer.hpp"
#include "rti/topic/TopicTraits.hpp"
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

class NDDSUSERDllExport ICLab {

  public:
    ICLab();

    ICLab(
        uint16_t num_of_student_param,
        const dds::core::string& head_param,
        const dds::core::string& pet_param,
        const dds::core::vector<dds::core::string>& student_param);

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    ICLab (ICLab&&) = default;
    ICLab& operator=(ICLab&&) = default;
    ICLab& operator=(const ICLab&) = default;
    ICLab(const ICLab&) = default;
    #else
    ICLab(ICLab&& other_) OMG_NOEXCEPT;  
    ICLab& operator=(ICLab&&  other_) OMG_NOEXCEPT;
    #endif
    #endif 

    uint16_t num_of_student() const OMG_NOEXCEPT;
    void num_of_student(uint16_t value);

    dds::core::string& head() OMG_NOEXCEPT; 
    const dds::core::string& head() const OMG_NOEXCEPT;
    void head(const dds::core::string& value);

    dds::core::string& pet() OMG_NOEXCEPT; 
    const dds::core::string& pet() const OMG_NOEXCEPT;
    void pet(const dds::core::string& value);

    dds::core::vector<dds::core::string>& student() OMG_NOEXCEPT; 
    const dds::core::vector<dds::core::string>& student() const OMG_NOEXCEPT;
    void student(const dds::core::vector<dds::core::string>& value);

    bool operator == (const ICLab& other_) const;
    bool operator != (const ICLab& other_) const;

    void swap(ICLab& other_) OMG_NOEXCEPT;

  private:

    uint16_t m_num_of_student_;
    dds::core::string m_head_;
    dds::core::string m_pet_;
    dds::core::vector<dds::core::string> m_student_;

};

inline void swap(ICLab& a, ICLab& b)  OMG_NOEXCEPT 
{
    a.swap(b);
}

NDDSUSERDllExport std::ostream& operator<<(std::ostream& o,const ICLab& sample);

namespace dds { 
    namespace topic {

        template<>
        struct topic_type_name<ICLab> {
            NDDSUSERDllExport static std::string value() {
                return "ICLab";
            }
        };

        template<>
        struct is_topic_type<ICLab> : public dds::core::true_type {};

        template<>
        struct topic_type_support<ICLab> {

            NDDSUSERDllExport static void initialize_sample(ICLab& sample);

            NDDSUSERDllExport static void register_type(
                dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, const ICLab& sample);

            NDDSUSERDllExport static void from_cdr_buffer(ICLab& sample, const std::vector<char>& buffer);

            static const rti::topic::TypePluginKind::type type_plugin_kind = 
            rti::topic::TypePluginKind::NON_STL;
        };

    }
}

namespace rti { 
    namespace topic {
        template<>
        struct dynamic_type<ICLab> {
            typedef dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const dds::core::xtypes::StructType& get();
        };

        template<>
        struct impl_type<ICLab> {
            typedef ICLab_c type;
        };

    }
}

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif // Example2_31567102_hpp

