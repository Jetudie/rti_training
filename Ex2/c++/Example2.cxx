

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example2.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include <iosfwd>
#include <iomanip>
#include "Example2.hpp"
#include "Example2ImplPlugin.h"

#include <rti/util/ostream_operators.hpp>

// ---- ICLab: 

ICLab::ICLab() :
    m_num_of_student_ (0) {
}   

ICLab::ICLab (
    uint16_t num_of_student_param,
    const dds::core::string& head_param,
    const dds::core::string& pet_param,
    const dds::core::vector<dds::core::string>& student_param)
    :
        m_num_of_student_( num_of_student_param ),
        m_head_( head_param ),
        m_pet_( pet_param ),
        m_student_( student_param ) {
}
#ifdef RTI_CXX11_RVALUE_REFERENCES
#ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
ICLab::ICLab(ICLab&& other_) OMG_NOEXCEPT  :m_num_of_student_ (std::move(other_.m_num_of_student_))
,
m_head_ (std::move(other_.m_head_))
,
m_pet_ (std::move(other_.m_pet_))
,
m_student_ (std::move(other_.m_student_))
{
} 

ICLab& ICLab::operator=(ICLab&&  other_) OMG_NOEXCEPT {
    ICLab tmp(std::move(other_));
    swap(tmp); 
    return *this;
}
#endif
#endif   

void ICLab::swap(ICLab& other_)  OMG_NOEXCEPT 
{
    using std::swap;
    swap(m_num_of_student_, other_.m_num_of_student_);
    swap(m_head_, other_.m_head_);
    swap(m_pet_, other_.m_pet_);
    swap(m_student_, other_.m_student_);
}  

bool ICLab::operator == (const ICLab& other_) const {
    if (m_num_of_student_ != other_.m_num_of_student_) {
        return false;
    }
    if (m_head_ != other_.m_head_) {
        return false;
    }
    if (m_pet_ != other_.m_pet_) {
        return false;
    }
    if (m_student_ != other_.m_student_) {
        return false;
    }
    return true;
}
bool ICLab::operator != (const ICLab& other_) const {
    return !this->operator ==(other_);
}

// --- Getters and Setters: -------------------------------------------------
uint16_t ICLab::num_of_student() const OMG_NOEXCEPT{
    return m_num_of_student_;
}

void ICLab::num_of_student(uint16_t value) {
    m_num_of_student_ = value;
}

dds::core::string& ICLab::head() OMG_NOEXCEPT {
    return m_head_;
}

const dds::core::string& ICLab::head() const OMG_NOEXCEPT {
    return m_head_;
}

void ICLab::head(const dds::core::string& value) {
    m_head_ = value;
}

dds::core::string& ICLab::pet() OMG_NOEXCEPT {
    return m_pet_;
}

const dds::core::string& ICLab::pet() const OMG_NOEXCEPT {
    return m_pet_;
}

void ICLab::pet(const dds::core::string& value) {
    m_pet_ = value;
}

dds::core::vector<dds::core::string>& ICLab::student() OMG_NOEXCEPT {
    return m_student_;
}

const dds::core::vector<dds::core::string>& ICLab::student() const OMG_NOEXCEPT {
    return m_student_;
}

void ICLab::student(const dds::core::vector<dds::core::string>& value) {
    m_student_ = value;
}

std::ostream& operator << (std::ostream& o,const ICLab& sample)
{
    rti::util::StreamFlagSaver flag_saver (o);
    o <<"[";
    o << "num_of_student: " << sample.num_of_student()<<", ";
    o << "head: " << sample.head()<<", ";
    o << "pet: " << sample.pet()<<", ";
    o << "student: " << sample.student() ;
    o <<"]";
    return o;
}

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        const dds::core::xtypes::StructType& dynamic_type<ICLab>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(ICLab_c_get_typecode())));
        }

    }
}  

namespace dds { 
    namespace topic {
        void topic_type_support<ICLab>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name){

            rti::domain::register_type_plugin(
                participant,
                type_name,
                ICLab_cPlugin_new,
                ICLab_cPlugin_delete);
        }

        void topic_type_support<ICLab>::initialize_sample(ICLab& sample){

            ICLab_c* native_sample=reinterpret_cast<ICLab_c*> (&sample);

            struct DDS_TypeDeallocationParams_t deAllocParams = {RTI_FALSE, RTI_FALSE};
            ICLab_c_finalize_w_params(native_sample,&deAllocParams);

            struct DDS_TypeAllocationParams_t allocParams = {RTI_FALSE, RTI_FALSE, RTI_TRUE}; 
            RTIBool ok=ICLab_c_initialize_w_params(native_sample,&allocParams);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to initialize_w_params");

        } 

        std::vector<char>& topic_type_support<ICLab>::to_cdr_buffer(
            std::vector<char>& buffer, const ICLab& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = ICLab_cPlugin_serialize_to_cdr_buffer(
                NULL, &length,reinterpret_cast<const ICLab_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = ICLab_cPlugin_serialize_to_cdr_buffer(
                &buffer[0], &length, reinterpret_cast<const ICLab_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;

        }

        void topic_type_support<ICLab>::from_cdr_buffer(ICLab& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = ICLab_cPlugin_deserialize_from_cdr_buffer(
                reinterpret_cast<ICLab_c*> (&sample), &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to create ICLab from cdr buffer");
        }

    }
}  

